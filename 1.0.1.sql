
CREATE TABLE schema_version(
    id            INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    major_version INTEGER NOT NULL,
    minor_version INTEGER NOT NULL,
    build_version INTEGER NOT NULL,
    name          VARCHAR NOT NULL,
    applied_at    DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT version UNIQUE (major_version, minor_version, build_version)
);

INSERT INTO schema_version (
    major_version,
    minor_version,
    build_version,
    name) 
    VALUES (1, 0, 1, '1.0.1');

CREATE TABLE characters(
    userid  VARCHAR NOT NULL,
    name    VARCHAR NOT NULL,
    gender  VARCHAR NOT NULL CHECK( gender = 'M' 
                                 OR gender = 'F'
                                 OR gender = 'O'),
    age     INTEGER NOT NULL,
    CONSTRAINT pk_user PRIMARY KEY (userid, name)
    );

CREATE TABLE items(
    userid VARCHAR NOT NULL REFERENCES characters(userid) ON DELETE CASCADE,
    c_name VARCHAR NOT NULL REFERENCES characters(name) ON DELETE CASCADE,
    name   VARCHAR NOT NULL,
    qntity INTEGER NOT NULL,
    CONSTRAINT pk_item PRIMARY KEY (userid, c_name, name)
    );